#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask,render_template,request
from pymongo import MongoClient
from bson.objectid import ObjectId


app = Flask(__name__)

client = MongoClient('172.16.0.22', 27017, connect=False)
db = client.opros
collopros = db.opros
collsipid = db.sip_id
collpc = db.pc

@app.route('/call', methods=['GET'])
def callid():
    city = str('нет в списке')
    callid = str(request.args.get('callid'))
    user = str(request.args.get('user'))
    pc = ''


    print ('  return incomming url from support pc ' + callid + ' ' + user)
#--- ищем город по номеру
    
    query = {"_id": ObjectId(callid) }
    for value in collopros.find(query):
        resultsipid = value['sip_id']
    
    query = {"sip_id": resultsipid }
    for value in collsipid.find(query):
        city = value['name']
    
    query = {"callid": resultsipid}
    for value in collpc.find(query):
        pc = value['pc']
    
    query = { 'themes': {'$exists': True}, 'sip_id': resultsipid }

    history_call = collopros.find_one(query, sort=[( '_id', -1 )])

        
    return render_template('main.html', 
            city=city,
            callid=callid,
            collection=collopros,
            resultsipid=resultsipid,
            user=user,
            pc=pc,
            history_call=history_call
            )

        







