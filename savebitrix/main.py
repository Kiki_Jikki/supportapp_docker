#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask,render_template,request
import datetime, requests
from pymongo import MongoClient
from urllib.parse import quote_plus as quote
from bson.objectid import ObjectId
from config import bxtoken, server_link
from bitrix24 import *
from werkzeug.debug import DebuggedApplication
from bs4 import BeautifulSoup as b
import os
import uuid

UPLOAD_FOLDER = '/var/www/telegram'

app = Flask(__name__)
app.config["CACHE_TYPE"] = "null"
application = DebuggedApplication(app, evalex=False)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
client = MongoClient('172.16.0.22', 27017, connect=False)
db = client.opros
collopros = db.opros
collsipid = db.sip_id
collusers = db.users
collpc = db.pc
bx24=Bitrix24(bxtoken)

def quote():
    r = requests.get('http://ibash.org.ru/random.php')
    html = b(r.text,'html.parser')
    citt = html.find('div', class_='quote')
    quote = citt.text
    return quote


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/savebitrix', methods=['POST'])
def savebitrix():
    if request.method == 'POST':
#----   достаем данные из урла
               
        print('   was commit bitrix task')
        
        status_task = 2
        city = str(request.args.get('city'))
        callid = str(request.args.get('callid'))
        resultsipid = str(request.args.get('sip'))
        user = str(request.args.get('user'))
#----  достаем данные из форм

        worker = str(request.form.get('worker'))
        comment = str(request.form.get('comment'))
        header = request.form.getlist('checkbox')
        if request.form.getlist('close_task'):
            status_task = 5  
        hostname = str(request.form.get('hostname')) 
        header = str(",".join(header))
        query = {"user": user }
        for value in collusers.find(query):
            resultbitrixid = str(value['bitrixid'])
            resultuserfordb = str(value['userfordb'])

#----   сохраняем файл
        
        file = request.files['file']
        if file and allowed_file(file.filename):
            f = request.files['file'] 
            filename = str(uuid.uuid4()) + '.jpg'
            f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            photo_link = str(server_link + filename)
            description_photo = "\n \n Ссылка на фотографию: " + photo_link
        else:
            description_photo = " "

#----- пробуем сделать себя по дефолту воркером
        
        print('   return variables from url bitrix task')

        if worker == '999':
            worker = resultbitrixid

        print ('   find self worker  ' + worker )

# ---- Меняем данные о звонке согласно опросу

        query = {"_id": ObjectId(callid) }
        result = { "$set": { 'sip_id': resultsipid, 'user': resultuserfordb, 'themes': header, 'city': city } }
        modify = collopros.update_one(query,result)

        print ('   modify data to db ' )       

        description = "Город/Отдел:  " + city + " \n Телефон:  " + resultsipid + " \n Имя компьютера: " + hostname + "\n " + comment + description_photo
        description = str(description)
        project = "42"
        query = {"name": city }
        for value in collsipid.find(query):
            if value:
                if 'project' in value:
                    project = str(value['project'])
            
        auditor = [732]
   
        query = { "callid": resultsipid }
        findresult = collpc.find_one(query)
        
        if findresult:
            result = { "$set": {'pc': hostname }}
            modify = collpc.update_one(query,result)
        else:
            insert = collpc.insert_one({"callid": resultsipid, "pc": hostname})
    
#----- наш любимый битрикс
        now = datetime.datetime.now()
        date_time = now.strftime("%y-%m-%d")
        date_time = str(date_time + " 20:00")
        taskstatus = 'Заявка сохранена'
        color = 'black'
        worker = str(worker)
       
        if 'Бесполезный звонок' in header or 'Повторный звонок' in header:
            print('   useless call option selected')
            taskstatus = 'Заявка не сохранена'
            color = 'white'
        else:
            try:
                print('status_task ', status_task )
                bx24.callMethod('tasks.task.add', fields={
                    'TITLE': header, 
                    'DESCRIPTION': description, 
                    'CREATED_BY': resultbitrixid, 
                    'AUDITORS': auditor, 
                    'RESPONSIBLE_ID': worker, 
                    'GROUP_ID': project , 
                    'DEADLINE': date_time,
                    'STATUS': status_task
                    })
                print ('   insert data to bitrix END')
            except BitrixError as error1:
                print(BitrixError)
                taskstatus = 'Всё плохо, заявка не сохранилась'
                color = 'red'
                print ('   ERROR insert data to bitrix END')
        
    return render_template('savebitrix.html',
            color=color,
            comment=comment,
            taskstatus = taskstatus,
            city=city,
            header=header,
            sip=resultsipid,
            callid=callid,
            quote = quote()
            )


        







