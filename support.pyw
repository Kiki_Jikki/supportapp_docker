import tkinter as tk
from tkinter import messagebox
import pystray
import webbrowser
from win10toast_click import ToastNotifier
import socket
import time



#------------------------
global user

def quit_window(icon,item):
    icon.stop()
    window.destroy()

def show_window(icon,item):
    icon.stop()
    window.after(0,window.deiconify)

def open_download(download_page):
    try:
        webbrowser.open(download_page)
    except:
        print("Ошибка открытия страницы")

def listenport(host: str = '', port: int = 23927):

    while True:
        s =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((host, port))
        s.listen(10)
        conn, addr = s.accept()
        with conn:
            print('connected by', addr)
            while True:
                data = conn.recv(1024)
                data1 = {data.decode()}
                print ('data = ' + str(data1))
                if not data: break
                conn.sendall(data)
                callid = data1

        
        if callid == {'ping'}:
            print('ping ok')
            s.close()
        else:
            s.close()
            opensite(callid,user)


def opensite(callid,user):
    download_page = f'http://172.16.0.22:8003/call?callid={callid}&user={user}'
    
    download_page1 = download_page.replace('{','')
    download_page2 = download_page1.replace('}','')
    download_page3 = download_page2.replace("'",'')
   
    notify(download_page3)
    

def notify(download_page3):
    toast = ToastNotifier()
    toast.show_toast("Входящий Звонок", "Дзынь дзынь", duration=1, callback_on_click=open_download(download_page3))

def withdraw_windows():
    window.withdraw()
    menu = (item('Quit', quit_window), item('Show', show_window))
    icon = pystray.Icon("name", image, "title", menu)
    icon.run()

def user1():
    global user
    user="lihacheva"
    print (user)
def user2():
    global user
    user="myagkova"
    print (user)
def user3():
    global user
    user="shestakov"
    print (user)
def user4():
    global user
    user="lopatina"
    print (user)


def switch():
    btn1["state"] = 'disabled'
    btn2["state"] = 'disabled'
    btn3["state"] = 'disabled'
    btn4["state"] = 'disabled'

def combine_funcs(*funcs):
    def combined_func(*args, **kwargs):
        for f in funcs:
            f(*args, **kwargs)
    return combined_func

def on_closing():
    if messagebox.askokcancel("Выход", "Пора домой?"):
        win.destroy()



#-------------------

        
win = tk.Tk()
win.geometry(f"1000x600+100+200")
win.title('Support Application')
label_1 = tk.Label(win,
                   text='Ты кто такой?',
                   font=('Arial',24,'bold'),
                   pady=20)


btn1 = tk.Button(win,text='Лена Лихачева',
                 command= combine_funcs(user1,switch,listenport),
                 font=('Arial',20)
                                  )
btn2 = tk.Button(win,text='Ульяна Мягкова',
                 command= combine_funcs(user2,switch,listenport),
                 font=('Arial',20)
                 )
btn3 = tk.Button(win,text='Александр Шестаков',
                 command= combine_funcs(user3,switch,listenport),
                 font=('Arial',20)
                 )
btn4 = tk.Button(win,text='Наталья Лопатина',
                 command= combine_funcs(user4,switch,listenport),
                 font=('Arial',20)
                 )


win.grid_columnconfigure(0, minsize=100)
win.grid_columnconfigure(1, minsize=100)


label_1.pack()
btn1.pack()
btn2.pack()
btn3.pack()
btn4.pack()

win.protocol("WM_DELETE_WINDOW", on_closing)
win.mainloop()
