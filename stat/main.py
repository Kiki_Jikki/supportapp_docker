#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask,render_template
import datetime, requests, re
from pymongo import MongoClient
from themes import themes
from config import *
from bitrix24 import *
from cities import cities

app = Flask(__name__)
app.config["CACHE_TYPE"] = "null"
client = MongoClient('172.16.0.22', 27017, connect=False)
db = client.opros
collopros = db.opros
collusers = db.users
collmissed = db.missed_call
bx24=Bitrix24(bxtoken)


@app.route('/stat')
def stat():
    
    session = requests.Session()
    session.headers.update(headers)
    response = session.post(login_url, data=data)
    
    now = datetime.datetime.now()
    print('user visit to STATISTICS site')
    resUser = {}
    resUserDay = {}
    allCount = 0
    users = {}
    usersOpenTask = {}
    usersDelegationTask = {}
    userCreatedTask = {}
    resThemes = {}
    date = now.strftime("%d-%m-%y")
    date_month = now.strftime("%m-%y")
   
    def parse_bitrix(url):
        parsedata = session.get(url).text
        result = re.search(start + '(..+?)' + end, parsedata)
        if result:
            found = result.group(1)
        else:
            result = re.search(start + '(...+?)' + end, parsedata)
            if result:
                found = result.group(1)
            else:
                fount = 0
        return found


    query = {"name": "counter"}
    for value in collmissed.find(query):
        missed_day = value['day']
        missed_month = value['month']


# запрос в битрикс о задачах пользователя

    query = {'user': {'$exists': 'true'}}
    for value in collusers.find(query, {'_id': 0, 'userfordb': 1, 'bitrixid': 1, 'url_complited': 1, 'url_all_tasks': 1}):
        users[value['userfordb']] = parse_bitrix(value['url_complited'])
        userCreatedTask[value['userfordb']] = parse_bitrix(value['url_all_tasks'])

        try:
            opentask = bx24.callMethod('tasks.task.list',
                    filter={
                        'REAL_STATUS': 2,
                        'RESPONSIBLE_ID': value['bitrixid'],
                        'AUDITOR': 732
                        },
                    select=['ID', 'STAGE_ID', 'PROBABILITY', 'CREATED_DATE'],
                    ORDER={'ID': 'asc'})
        except BitrixError as message:
            print(message)

        usersOpenTask[value['userfordb']] = len(opentask['tasks'])

# количество звонков по сотрудникам за месяц

    for userCount in users:
        query = {"user": userCount, "date": {'$regex': date_month}}
        resUser[userCount] = collopros.count_documents(query)
        allCount = allCount + collopros.count_documents(query)
        query = {"user": userCount, "date": {'$regex': date}}
        resUserDay[userCount] = collopros.count_documents(query)
        usersDelegationTask[userCount] = int(userCreatedTask[userCount]) - int(users[userCount]) - int(usersOpenTask[userCount])
   
 # количество звонок по темам

    for themesCount in themes:
        query = {"themes": {'$regex': themesCount}, "date": {'$regex': date_month}}
        resThemes[themesCount] = collopros.count_documents(query)
    
    resThemes = dict(sorted(resThemes.items(), reverse=True, key= lambda item: item[1]))    

    return render_template('stat.html', 
            resUser = resUser, 
            resThemes = resThemes, 
            themesCount = themesCount, 
            allCount = allCount,
            resUserDay=resUserDay,
            usersOpenTask=usersOpenTask,
            usersDelegationTask=usersDelegationTask,
            userCreatedTask=userCreatedTask,
            users=users,
            resCity=cities,
            missed_day=missed_day,
            missed_month=missed_month
            )
            




