#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask,request
import socket, datetime
from pymongo import MongoClient


app = Flask(__name__)

client = MongoClient('172.16.0.22', 27017, connect=False)
db = client.opros
collopros = db.opros
colltechpc = db.techpc

@app.route('/iptel', methods=['GET'])
def iptel():

#--- вылавливаем данные с урла
    
    now = datetime.datetime.now()
    incall = str(request.args.get('incall'))
    techuser = str(request.args.get('techuser'))
    res_str = incall.replace('sip:', '')
    sep = '@'
    rest = res_str.split(sep, 1)[0]
    incall = rest
#--- добавляем запись звонка в коллекцию opros
    
    query = {"techuser": techuser }
    for value in colltechpc.find(query):
        resulttechpc = str(value['techpc'])

    result = collopros.insert_one(
            {
                "sip_id": incall,
                "date": now.strftime("%d-%m-%y %H:%M"),
                }
            )
    callid = str(result.inserted_id)

#--- перенедаем данные на комп техподдержки

    sock = socket.socket()
    sock.connect((resulttechpc,23927))
    data = callid.encode()
    sock.sendall(bytes(data))
    data = sock.recv(1024)
    sock.close()
    return ('iptel function end')
