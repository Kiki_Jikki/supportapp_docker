#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pymongo import MongoClient

client = MongoClient('172.16.0.22', 27017, connect=False)
db = client.opros
collmissed = db.missed_call


query = {"name": "counter"}
date = {"$set": { 'last': 0, 'day': 0 }}
modify = collmissed.update_one(query,date)
