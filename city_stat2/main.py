#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask,render_template, request
import datetime
from pymongo import MongoClient
from themes import themes


app = Flask(__name__)
client = MongoClient('172.16.0.22', 27017, connect=False)
db = client.opros
collopros = db.opros

@app.route('/statcity', methods=['POST'])
def statcity():
    if request.method == 'POST':
        city = request.form.get('selectcity')
        resThemes = {}

        if request.form.getlist('last_month'):
            today = datetime.date.today()
            first = today.replace(day=1)
            date_month = first - datetime.timedelta(days=1)
            date_month = date_month.strftime("%m-%y")
            date = now.strftime("%d-%m-%y")
        else:
            now = datetime.datetime.now()
            date_month = now.strftime("%m-%y")
            date = now.strftime("%d-%m-%y")


        for themesCount in themes:
            query = {"themes": {'$regex': themesCount}, "date": {'$regex': date_month}, "city": {'$regex': city}}
            resThemes[themesCount] = collopros.count_documents(query)
    
        resThemes = dict(sorted(resThemes.items(), reverse=True, key= lambda item: item[1]))
    return render_template('stat.html', 
            resThemes = resThemes, 
            themesCount = themesCount,
            city = city
            )
            




