#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pymongo import MongoClient

client = MongoClient('172.16.0.22', 27017, connect=False)
db = client.opros
collmissed = db.missed_call


# узнаем число пропущенных за месяц и очищаем его
query = {"name": "counter"}
data = {"$set": { 'month': 0 }}
for value in collmissed.find(query):
    missed_month = value['month']
    print(missed_month)
modify = collmissed.update_one(query, data)

# передаем информацию о предыдущем месяце
query = {"name": "last_counter"}
data = {"$set": { 'month': missed_month }}
modify = collmissed.update_one(query,data)
