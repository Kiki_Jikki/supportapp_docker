#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pymongo import MongoClient
import datetime 

client = MongoClient('172.16.0.22', 27017, connect=False)
db = client.opros
collusers = db.users


today = datetime.date.today()
first = today.replace(day=1)
str_first = first.strftime("%d.%m.%Y")

last_day_last_month = first - datetime.timedelta(days=1)
str_last_day_last_month = last_day_last_month.strftime("%d.%m.%Y")

first_day_last_month = last_day_last_month.replace(day=1)
str_first_day_last_month = first_day_last_month.strftime("%d.%m.%Y")

last_day_before_last_month = first_day_last_month - datetime.timedelta(days=1)
str_last_day_before_last_month = last_day_before_last_month.strftime("%d.%m.%Y")

first_day_before_last_month = last_day_before_last_month.replace(day=1)
str_first_day_before_last_month = first_day_before_last_month.strftime("%d.%m.%Y")

query = {"user": {'$exists': 'true', 'user': 'lihacheva'}}

for value in collusers.find(query, {'_id': 0, 'url_complited': 1, 'url_all_tasks': 1, 'last_url_all_tasks': 1, 'last_url_complited': 1}):
    url_complited = value['url_complited']
    url_all_tasks = value['url_all_tasks']
    last_url_complited = value['last_url_complited']
    last_url_all_tasks = value['last_url_all_tasks']
    
    url_complited = url_complited.replace(str_first_day_last_month,str_first)
    url_all_tasks = url_all_tasks.replace(str_first_day_last_month,str_first)
    last_url_complited = last_url_complited.replace(str_first_day_before_last_month,str_first_day_last_month)
    last_url_complited = last_url_complited.replace(str_last_day_before_last_month,str_last_day_last_month)
    last_url_all_tasks = last_url_all_tasks.replace(str_first_day_before_last_month,str_first_day_last_month)
    last_url_all_tasks = last_url_all_tasks.replace(str_last_day_before_last_month,str_last_day_last_month)

    data = {"$set": { 
        'url_complited': url_complited,
        'url_all_tasks': url_all_tasks,
        'last_url_complited': last_url_complited,
        'last_url_all_tasks': last_url_all_tasks }}
    
    modify = collusers.update_one(query,data)




