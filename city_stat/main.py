#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask,render_template,request
import datetime
from pymongo import MongoClient
from cities import cities


app = Flask(__name__)
client = MongoClient('172.16.0.22', 27017, connect=False)
db = client.opros
collopros = db.opros


@app.route('/stat', methods=['POST'])
def stat():
    if request.method == 'POST':
        resCity = {}
        now = datetime.datetime.now()
        
        if request.form.getlist('last_month'):
            today = datetime.date.today()
            first = today.replace(day=1)
            date_month = first - datetime.timedelta(days=1)
            date_month = date_month.strftime("%m-%y")
            date = now.strftime("%d-%m-%y")
        else:
            date_month = now.strftime("%m-%y")
            date = now.strftime("%d-%m-%y")
        
        for cityCount in cities:
            query = {"city": {'$regex': cityCount}, "date": {'$regex': date_month}}
            resCity[cityCount] = collopros.count_documents(query)
        resCity = dict(sorted(resCity.items(), reverse=True, key= lambda item: item[1]))
    return render_template('stat.html', 
            resCity = resCity, 
            cityCount = cityCount 
            )
            




