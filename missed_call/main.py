#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask,request
from pymongo import MongoClient

app = Flask(__name__)

client = MongoClient('172.16.0.22', 27017, connect=False)
db = client.opros
collmissed = db.missed_call

@app.route('/missedcall', methods=['GET'])
def iptel():
    
    incall = str(request.args.get('incall'))
    res_str = incall.replace('sip:', '')
    sep = '@'
    rest = res_str.split(sep, 1)[0]
    incall = rest
    
    techuser = str(request.args.get('techuser'))
    

    query = {"name": "counter"}
    for value in collmissed.find(query):
        missed_last = value['last']
        missed_day = value['day']
        missed_month = value['month']
        missed_techuser = value['techuser']

    if missed_last == incall and missed_techuser != techuser:
        missed_day = missed_day + 1
        missed_month = missed_month + 1
        date = {"$set": { 'last': 0, 'day': missed_day, 'month': missed_month, 'techuser': 0 }}
        modify = collmissed.update_one(query,date)
    else:
        date = {"$set": {'last': incall, 'techuser': techuser}}
        modify = collmissed.update_one(query,date)

    return ('missed call function end')
